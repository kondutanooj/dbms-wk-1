#include<stdio.h>
#include<stdlib.h>
void load(int id,char name[50], int age,int salary, char dept[50], char work[50]){
    FILE* file;
    file=fopen("MVGR_Employee.txt","a+");
    fprintf(file,"%d\t",id);
    fprintf(file,"%s\t",name);
    fprintf(file,"%d\t",age);
    fprintf(file,"%d\t",salary);
    fprintf(file,"%s\t",dept);
    fprintf(file,"%s\n",work);
    fclose(file);
}
int read() {
    FILE* file;
    char ch;
    file = fopen("MVGR_Employee.txt", "r");
    if (file == NULL) {
        printf("File can't be opened\n");
        return -1; 
    }
    do {
        ch = fgetc(file);
        if (ch != EOF) {
            printf("%c", ch);
        }
    } while (ch != EOF);
    fclose(file);
    return 0;
}
void main(){
    int id,age,salary;
    char name[50],dept[50],work[50];
    int op;
    while(1){
        printf("1) Enter Employee Data \n2) Employee Data \n3) Exit\n");
        printf("Enter your choice: \n");
        scanf("%d",&op);
        switch(op){
            case 1: printf("Enter the Employee_ID : ");
                    scanf("%d",&id);
                    printf("Enter the Employee_Name : ");
                    scanf("%s",name);
                    printf("Enter the Employee_Age : ");
                    scanf("%d",&age);
                    printf("Enter the Employee_Salary : ");
                    scanf("%d",&salary);
                    printf("Enter the Employee_Department : ");
                    scanf("%s",dept);
                    printf("Enter the Employee_Profession : ");
                    scanf("%s",work);
                    load(id,name,age,salary,dept,work);
                    break;
            case 2: read(); break;
            case 3: exit(1); break;
            default: printf("Invalid Input !!!\n");
        }
    }
}